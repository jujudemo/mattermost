FROM ubuntu:16.04

# This is terribly hacky
ENV GOPATH=/root/go
ENV PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:$GOPATH/bin
ENV JENKINS_ID=113
RUN apt-get update && apt-get install -y build-essential golang curl git libpng12-0 zip
RUN curl https://get.docker.com/ | /bin/bash
RUN mkdir /root/go

# SET file limits
RUN ulimit -n 8096

RUN curl -sL https://deb.nodesource.com/setup_6.x | bash -

RUN apt-get install -y nodejs
WORKDIR /root/go


RUN mkdir -p src/github.com/mattermost
VOLUME /root/go/src/github.com/mattermost/platform
WORKDIR /root/go/src/github.com/mattermost/platform
ENTRYPOINT make clean && make dist && chown -R $JENKINS_ID .
